# Web API with Hibernate - Movie Characters

## API Documentation

## Swagger UI
http://localhost:8080/swagger-ui.html
# Functionality

## Character
- id
- title
- genre
- releaseYear
- director
- pictureURL
- trailer
- franchise
- movieCharacters

### Endpoints

#### Get all characters

`GET /api/characters`

#### Create character

`POST /api/characters/{id}"`

#### Get character by id

`GET /api/characters/{id}"`

#### Update character by id

`PUT /api/characters/{id}"`

#### Delete character by id

`DELETE /api/characters/{id}"`

## Movie
- id
- title
- genre
- releaseYear
- director
- pictureURL
- trailer
- franchise
- movieCharacters

### Endpoints

#### Get all movies

`GET /api/movies`

#### Create movie

`POST /api/movies/{id}"`

#### Get movie by id

`GET /api/movies/{id}"`

#### Update movie by id

`PUT /api/movies/{id}"`

#### Delete movie by id

`DELETE /api/movies/{id}"`

## Franchise
- id
- fullName
- alias
- gender
- pictureURL
- movies

### Endpoints

#### Get all franchises

`GET /api/franchises`

#### Create franchise

`POST /api/franchises/{id}"`

#### Get franchise by id

`GET /api/franchises/{id}"`

#### Update franchise by id

`PUT /api/franchises/{id}"`

#### Delete franchise by id

`DELETE /api/franchises/{id}"`







