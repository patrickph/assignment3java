package com.assignment.Assignment3Java;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Assignment3JavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(Assignment3JavaApplication.class, args);
	}

}
