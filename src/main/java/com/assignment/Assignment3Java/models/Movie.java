package com.assignment.Assignment3Java.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String title;

    @Column
    private String genre;

    @Column
    private String releaseYear;

    @Column
    private String director;

    @Column
    private String pictureURL;

    @Column
    private String trailer;

    @ManyToOne
    @JoinColumn(name = "franchise_id")
    private Franchise franchise;

    @JsonGetter("franchise")
    public Long franchiseGetter() {
        if (franchise != null) {
            return franchise.getId();
        }
        return null;
    }

    @ManyToMany
    @JoinTable(
            name = "movie_character_movie",
            joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "character_id")}
    )
    private Set<MovieCharacter> movieCharacters;

    @JsonGetter("movieCharacters")
    public List<Long> movieCharactersGetter() {
        if (movieCharacters != null) {
            return movieCharacters.stream().map(MovieCharacter::getId).collect(Collectors.toList());
        }
        return null;
    }

    public Movie(String title, String genre, String releaseYear, String director, String pictureURL, String trailer) {
        this.title = title;
        this.genre = genre;
        this.releaseYear = releaseYear;
        this.director = director;
        this.pictureURL = pictureURL;
        this.trailer = trailer;
    }

    public Movie() {
    }

    public Franchise getFranchise() {
        return franchise;
    }

    public void setFranchise(Franchise franchise) {
        this.franchise = franchise;
    }

    public Set<MovieCharacter> getMovieCharacters() {
        return movieCharacters;
    }

    public void setMovieCharacters(Set<MovieCharacter> movieCharacters) {
        this.movieCharacters = movieCharacters;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(String releaseYear) {
        this.releaseYear = releaseYear;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }

    public String getTrailer() {
        return trailer;
    }

    public void setTrailer(String trailer) {
        this.trailer = trailer;
    }
}
