package com.assignment.Assignment3Java.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class MovieCharacter {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String fullName;

    @Column
    private String alias;

    @Column
    private String gender;

    @Column
    private String pictureURL;

    @ManyToMany(mappedBy = "movieCharacters")
    private Set<Movie> movies;

    @JsonGetter("movies")
    public List<Long> moviesGetter() {
        if (movies != null) {
            return movies.stream().map(Movie::getId).collect(Collectors.toList());
        }
        return null;
    }

    public MovieCharacter(String fullName, String alias, String gender, String pictureURL) {
        this.fullName = fullName;
        this.alias = alias;
        this.gender = gender;
        this.pictureURL = pictureURL;
    }

    public MovieCharacter() {
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public void setPictureURL(String pictureURL) {
        this.pictureURL = pictureURL;
    }
}
