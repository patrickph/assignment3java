package com.assignment.Assignment3Java.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String description;

    @OneToMany
    @JoinColumn(name = "franchise_id")
    private Set<Movie> movies;

    @JsonGetter("movies")
    public List<Long> moviesGetter() {
        if (movies != null) {
            return movies.stream().map(Movie::getId).collect(Collectors.toList());
        }
        return null;
    }

    public Franchise(String name, String description) {
        this.name = name;
        this.description = description;
        movies = new HashSet<>();
    }

    public Franchise() {
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
