package com.assignment.Assignment3Java;

import com.assignment.Assignment3Java.models.Franchise;
import com.assignment.Assignment3Java.models.Movie;
import com.assignment.Assignment3Java.models.MovieCharacter;
import com.assignment.Assignment3Java.repositories.FranchiseRepository;
import com.assignment.Assignment3Java.repositories.MovieCharacterRepository;
import com.assignment.Assignment3Java.repositories.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
class AppStartupRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(AppStartupRunner.class);

    @Autowired
    MovieCharacterRepository characterRepository;

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    FranchiseRepository franchiseRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.info("Your application started with option names : {}", args.getOptionNames());

        MovieCharacter bale = new MovieCharacter("Christian Bale", "Bale", "Male", "https://www.imdb.com/title/tt0468569/mediaviewer/rm4023877632/?ref_=tt_ov_i");
        MovieCharacter pitt = new MovieCharacter("Brad Pitt", "Brad", "Male", "https://www.google.com");
        MovieCharacter dan = new MovieCharacter("Dan Børge", "Børgis", "Male", "https://www.google.com");


        Movie sm1 = new Movie("Spider Man 1", "Thriller", "1998", "Unknown", null, null);
        Movie sm2 = new Movie("Spider Man 2", "Action", "2003", "Dan Børge", null, null);

        Franchise sm = new Franchise("Spider Man Universe", "Spider Man movies and stuff");

        if (characterRepository.count() == 0) {
            bale = characterRepository.save(bale);
            pitt = characterRepository.save(pitt);
            dan = characterRepository.save(dan);
        }

        Set<MovieCharacter> mc = new HashSet<>();
        mc.add(bale);
        mc.add(pitt);
        sm1.setMovieCharacters(mc);

        mc = new HashSet<>();
        mc.add(pitt);
        mc.add(dan);
        sm2.setMovieCharacters(mc);

        if (movieRepository.count() == 0) {
            sm1 = movieRepository.save(sm1);
            sm2 = movieRepository.save(sm2);
        }

        Set<Movie> movies = new HashSet<>();
        movies.add(sm1);
        movies.add(sm2);
        sm.setMovies(movies);

        if (franchiseRepository.count() == 0) {
            franchiseRepository.save(sm);
        }
    }
}