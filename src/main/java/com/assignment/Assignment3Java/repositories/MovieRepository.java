package com.assignment.Assignment3Java.repositories;

import com.assignment.Assignment3Java.models.Movie;
import com.assignment.Assignment3Java.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Set;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
}
