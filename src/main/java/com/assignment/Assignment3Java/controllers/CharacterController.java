package com.assignment.Assignment3Java.controllers;

import com.assignment.Assignment3Java.models.Movie;
import com.assignment.Assignment3Java.models.MovieCharacter;
import com.assignment.Assignment3Java.repositories.MovieCharacterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping("/api/characters")
public class CharacterController {

    @Autowired
    private MovieCharacterRepository characterRepository;

    @GetMapping
    public ResponseEntity<List<MovieCharacter>> getAllCharacters() {
        List<MovieCharacter> data = characterRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<MovieCharacter> getSpecificCharacter(@PathVariable Long id) {
        HttpStatus status;
        MovieCharacter character = new MovieCharacter();
        if(!characterRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(character, status);
        }
        character = characterRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(character, status);
    }

    @PostMapping
    public ResponseEntity<MovieCharacter> addCharacter(@RequestBody MovieCharacter character) {
        MovieCharacter mchar = characterRepository.save(character);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(mchar, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<MovieCharacter> updateCharacter(@RequestBody MovieCharacter character, @PathVariable Long id) {
        HttpStatus status;
        MovieCharacter mchar = new MovieCharacter();
        if(!Objects.equals(id, character.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(mchar, status);
        }
        mchar = characterRepository.save(character);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(mchar, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<MovieCharacter> deleteCharacter(@PathVariable Long id) {
        MovieCharacter character = characterRepository.findById(id).get();
        characterRepository.delete(character);
        return new ResponseEntity<>(character, HttpStatus.OK);
    }
}
