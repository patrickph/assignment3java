package com.assignment.Assignment3Java.controllers;

import com.assignment.Assignment3Java.models.Franchise;
import com.assignment.Assignment3Java.models.Movie;
import com.assignment.Assignment3Java.models.MovieCharacter;
import com.assignment.Assignment3Java.repositories.MovieCharacterRepository;
import com.assignment.Assignment3Java.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/api/movies")
public class MovieController {

    @Autowired
    private MovieRepository movieRepository;

    @Autowired
    private MovieCharacterRepository characterRepository;

    @GetMapping
    public ResponseEntity<List<Movie>> getAllMovies() {
        List<Movie> data = movieRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Movie> getSpecificMovie(@PathVariable Long id) {
        HttpStatus status;
        Movie movie = new Movie();
        if(!movieRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(movie, status);
        }
        movie = movieRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(movie, status);
    }

    @PostMapping
    public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
        Movie newMovie = movieRepository.save(movie);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(newMovie, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Movie> updateMovie(@RequestBody Movie movie, @PathVariable Long id) {
        HttpStatus status;
        Movie newMovie = new Movie();
        if(!Objects.equals(id, movie.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(newMovie, status);
        }
        newMovie = movieRepository.save(movie);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(newMovie, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Movie> deleteMovie(@PathVariable Long id) {
        Movie movie = movieRepository.findById(id).get();
        movieRepository.delete(movie);
        return new ResponseEntity<>(movie, HttpStatus.OK);
    }

    // Update characters in a movie
    @PutMapping("{id}/update")
    public ResponseEntity<Set<MovieCharacter>> updateCharactersInMovie(@RequestBody List<Long> charIDs, @PathVariable Long id) {
        Movie movie = movieRepository.findById(id).get();
        List<MovieCharacter> characters = characterRepository.findAllById(charIDs);
        movie.setMovieCharacters((Set<MovieCharacter>) characters);

        return new ResponseEntity<>((Set<MovieCharacter>)characters, HttpStatus.OK);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getAllCharacters(@PathVariable Long id) {
        Movie movie = movieRepository.findById(id).get();
        Set<MovieCharacter> movies = movie.getMovieCharacters();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }
}
