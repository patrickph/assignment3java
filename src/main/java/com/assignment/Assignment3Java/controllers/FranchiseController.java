package com.assignment.Assignment3Java.controllers;

import com.assignment.Assignment3Java.models.Franchise;
import com.assignment.Assignment3Java.models.Movie;
import com.assignment.Assignment3Java.models.MovieCharacter;
import com.assignment.Assignment3Java.repositories.FranchiseRepository;
import com.assignment.Assignment3Java.repositories.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/franchises")
public class FranchiseController {

    @Autowired
    private FranchiseRepository franchiseRepository;

    @Autowired
    private MovieRepository movieRepository;

    @GetMapping
    public ResponseEntity<List<Franchise>> getAllFranchises() {
        List<Franchise> data = franchiseRepository.findAll();
        HttpStatus status = HttpStatus.OK;
        return new ResponseEntity<>(data, status);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Franchise> getSpecificFranchise(@PathVariable Long id) {
        HttpStatus status;
        Franchise franchise = new Franchise();
        if(!franchiseRepository.existsById(id)){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(franchise, status);
        }
        franchise = franchiseRepository.findById(id).get();
        status = HttpStatus.OK;
        return new ResponseEntity<>(franchise, status);
    }

    @PostMapping
    public ResponseEntity<Franchise> addFranchise(@RequestBody Franchise franchise) {
        Franchise newFra = franchiseRepository.save(franchise);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(newFra, status);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Franchise> updateFranchise(@RequestBody Franchise franchise, @PathVariable Long id) {
        HttpStatus status;
        Franchise newFra = new Franchise();
        if(!Objects.equals(id, franchise.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(newFra, status);
        }
        newFra = franchiseRepository.save(franchise);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(newFra, status);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Franchise> deleteFranchise(@PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        franchiseRepository.delete(franchise);
        return new ResponseEntity<>(franchise, HttpStatus.OK);
    }

    @PutMapping("/{id}/update")
    public ResponseEntity<Set<Movie>> updateMoviesInFranchise(@RequestBody List<Long> movieIDs, @PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        List<Movie> movies = movieRepository.findAllById(movieIDs);
        franchise.setMovies((Set<Movie>) movies);

        return new ResponseEntity<>((Set<Movie>)movies, HttpStatus.OK);
    }

    @GetMapping("/{id}/movies")
    public ResponseEntity<Set<Movie>> getAllMovies(@PathVariable Long id) {
        Franchise franchise = franchiseRepository.findById(id).get();
        Set<Movie> movies = franchise.getMovies();
        return new ResponseEntity<>(movies, HttpStatus.OK);
    }

    @GetMapping("/{id}/characters")
    public ResponseEntity<Set<MovieCharacter>> getAllCharacters(@PathVariable Long id) {
        Set<Movie> movies = getAllMovies(id).getBody();
        List<Set<MovieCharacter>> characters = new ArrayList<>();
        characters = movies.stream().map(movie -> movie.getMovieCharacters()).collect(Collectors.toList());
        Set<MovieCharacter> allCharacters = characters.get(0);
        for (Set<MovieCharacter> c: characters) allCharacters.addAll(c);
        return new ResponseEntity<>(allCharacters, HttpStatus.OK);
    }
}
